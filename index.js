const express = require("express");

const app = express();

const port = 2000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get("/home", (req,res) =>{
	res.send("A simple message.");
});


let users = [];
app.post("/users", (req,res) => {
		console.log(req.body)
		if(req.body.username !== "" && req.body.password !== ""){
			users.push(req.body)
			res.send(`User ${req.body.username} is successfully registered!`);
		}else{
			res.send("Please input BOTH username and password.")
		}
})

app.get("/users", (req,res) => {
        res.send(users);
    });

app.delete("/delete-user", (req, res) => {
  let msg = "";
  if (users.length !== 0) {
    for (let i = 0; i < users.length; i++) {
      if (req.body.username === users[i].username) {
        msg = `User ${req.body.username} has been deleted!`;
        users.splice(i, 1);
        break;
      } else {
        req.body.username === ""
          ? (msg = "Please input username.")
          : (msg = "Username not found!");
      }
    }
    res.send(msg);
  } else res.send("There are no users registered yet!");
});


app.listen(port, () => console.log("Server running at port 2000."));
